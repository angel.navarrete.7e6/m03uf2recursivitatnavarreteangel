﻿/*
* AUTHOR: Angel Navarrete Sanchez
* DATE: 2023/01/17
* DESCRIPTION:  Menu amb conjunt de exercicis sobre recursivitat.
*/
using System;

namespace Recursivitat
{
    class Recursivitat
    {
        static void Main(string[] args)
        {
            Recursivitat inici = new Recursivitat();
            inici.Menu();
        }
        public void Menu()
        {

            MostrarOpcionsMenu();

            string opcio = DemanarOpcioMenu();
            EscollirOpcioMenu(opcio);
        }
        public void MostrarOpcionsMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("                                         Exercicis Recursivitat Angel Navarrete");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("\t1.- Factorial");
            Console.WriteLine("\t2.- DobleFactorial");
            Console.WriteLine("\t3.- NumDigits");
            Console.WriteLine("\t4.- NombresCreixents");
            Console.WriteLine("\t5.- ReduccioDigits");
            Console.WriteLine("\t6.- SequenciaAsteriscos");
            Console.WriteLine("\t7.- PrimersPerfectes");
            Console.WriteLine("\t8.- TorresHanoi");
            Console.WriteLine("\t0.- Sortir");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public string DemanarOpcioMenu()
        {
            Console.Write("Escull una opció: ");
            string opcio = Console.ReadLine();
            return opcio;
        }
        public void EscollirOpcioMenu(string opcio)
        {
            do
            {
                switch (opcio)
                {
                    case "1":
                        Exercici1Factorial();
                        break;
                    case "2":
                        Exercici2DobleFactorial();
                        break;
                    case "3":
                        NumDigits();
                        break;
                    case "4":
                        NombresCreixents();
                        break;
                    case "5":
                        ReduccioDigits();
                        break;
                    case "6":
                        SequenciaAsteriscos();
                        break;
                    case "7":
                        PrimersPerfectes();
                        break;
                    case "8":
                        TorresHanoi();
                        break;
                    case "0":
                        Console.WriteLine("Adeu");
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Opcio Incorrecta");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        break;
                }
                Console.ReadLine();
                Console.Clear();
                MostrarOpcionsMenu();
                opcio = DemanarOpcioMenu();
            } while (opcio != "0");
        }
        public int DemanarNumero()
        {
            int num;
            do
            {
                Console.Write("Introdueix un numero: ");
                num = Convert.ToInt32(Console.ReadLine());
            } while (num < 0);
            return num;
        }

        /*DESCRIPTION: Escriviu una funció que retorni n! Resoleu aquest problema recursivament.*/
        public void Exercici1Factorial() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------Factorial--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            int num = DemanarNumero();
            Console.WriteLine("El Factorial de "+ num + " es: "+Factorial(num));

        }

        int Factorial(int n)
        {
            if (n == 0 || n==1)return 1;
            else n *= Factorial(n - 1);
            return n;
        }

        
        /*DESCRIPTION: Escriviu una funció recursiva que retorni n!!. Recordeu que n!! = n × (n − 2) × (n − 4)*/
        void Exercici2DobleFactorial() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------DobleFactorial--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            uint num = (uint)DemanarNumero();
            Console.WriteLine("El doble factorial de " + num + " es " + DobleFactorial(num));
        }
        uint DobleFactorial(uint n)
        {
            if (n == 0 || n == 1) return 1; 
            else n *= DobleFactorial(n -2); 
            return n;
        }
        /*DESCRIPTION: Escriviu una funció recursiva que retorni el nombre de dígits de n.*/
        void NumDigits() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------NumDigits--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            int num = DemanarNumero();
            Console.WriteLine("El numero "+num+" te "+ContadorDigits(num)+" digits.");
        }

        public int ContadorDigits(int num)
        {
            int contador = 1;
            if (num<=9 && num>=0) return contador;
            else contador += ContadorDigits(num / 10); 
            return contador;
        }

        /*DESCRIPTION: Escriviu una funció recursiva que ens indiqui si un nombre és creixent o no. Un
            nombre és creixent si cada dígit és més petit o igual que el que està a la seva dreta.*/
        void NombresCreixents() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------NombresCreixents--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            int num = DemanarNumero();
            bool resultat;
            ComprovadorCreixents(num, out resultat);
            Console.WriteLine(resultat);
        }

        int ComprovadorCreixents(int num,out bool comprovador)
        {
            if(num <=9 && num >=0) comprovador = num < num*10;  
            else comprovador = num%10 >= ComprovadorCreixents(num/10, out comprovador); 
            return num%10;
        }

        /*DESCRIPTION: Feu una funció recursiva que, donat un natural x, retorni la reducció dels seus dígits.
            Direm que reduir els dígits d’un nombre consisteix a calcular la suma dels seus
            dígits. Si la suma és un dígit, aquest ja és el resultat. Altrament, es torna a aplicar el
            mateix procés a la suma obtinguda, fins a tenir un sol dígit.*/
        void ReduccioDigits() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------ReduccioDigits--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            int num = DemanarNumero();
            Console.WriteLine(ReductorDigit(num));
        }
        int ReductorDigit(int num)
        {
            int suma = 0;
            int residu;
            residu = num % 10;
            suma += num / 10 + residu;
            if (suma <= 9 && suma >= 0) return suma;
            else suma = ReductorDigit(suma);
            return suma;
        }
        /*DESCRIPTION: Feu un programa recursiu que llegeixi un natural n i que escrigui 2n − 1 barres amb
        asteriscos seguint el patró que es pot deduir dels exemples.*/
        void SequenciaAsteriscos() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------SequenciaAsteriscos--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            int num = DemanarNumero();
            FuncionAsteriscos(num);
        }
        int FuncionAsteriscos(int num)
        {
            if (num == 1) Console.WriteLine("*");
            else
            {
                FuncionAsteriscos(num - 1);
                ImprimirAsteriscos(num);
                num = FuncionAsteriscos(num - 1);

            }
            return num;
        }
        void ImprimirAsteriscos(int num)
        {
            for (int i = 0; i < num; i++)
            {
                Console.Write("*");
            }
            Console.WriteLine();
        }
        /*DESCRIPTION: Escriviu una funció recursiva que ens indiqui si un nombre és primer perfecte o no.
        Un primer perfecte ho és si totes les sumes dels seus dígits fins aquesta ser un
        nombre d’un dígit donen un nombre primer. Per exemple, 977 és un primer perfecte,
        perquè tant 977, com 9 + 7 + 7 = 23, com 2 + 3 = 5, com 5, ..., són tots nombres
        primers.*/
        void PrimersPerfectes() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------PrimersPerfectes--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            int num = DemanarNumero();
            Console.WriteLine(ComprovarPrimerPerfecte(num));
            
        }
        bool ComprovarPrimerPerfecte(int num)
        {
            bool primerPerfecte;
            int suma = 0;
            int residu;
            if (num <= 9 && num >= 0)
            {
                for (int i = 2; i < num; i++)
                {
                    if ((num % i) == 0)
                    {
                        primerPerfecte = false;
                        return primerPerfecte;
                    }
                }
                primerPerfecte = true;
            }
            else
            {
                
                residu = num % 10;
                suma += num / 10 + residu;
                //while (num != 0)
                //{
                //    suma += num % 10;
                //    num /= 10;                    
                //}
                primerPerfecte = ComprovarPrimerPerfecte(suma);
            }
            return primerPerfecte;
        }

        /*DESCRIPTION: Feu un programa que resolgui el joc de les torres de Hanoi, de manera que es facin
        el mínim nombre de moviments possibles.*/
        void TorresHanoi() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------TorresHanoi--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            int num = DemanarNumero();
            Hanoi(num,1,3,2);
        }
        void Hanoi(int num,int posInicial, int posFinal,int posAuxiliar)
        {
            if (num > 0)
            {
                Hanoi(num - 1, posInicial, posAuxiliar, posFinal);
                ImprimirResultatHanoi(posInicial, posFinal);
                Hanoi(num - 1, posAuxiliar, posFinal, posInicial);
            } 
        }
        void ImprimirResultatHanoi(int posInicial, int posFinal) {
            if (posInicial == 1 && posFinal == 3)
            {
                Console.WriteLine("A=>C");
            }
            else if (posInicial == 2 && posFinal == 3)
            {
                Console.WriteLine("B=>C");
            }
            else if (posInicial == 3 && posFinal == 2)
            {
                Console.WriteLine("C=>B");
            }
            else if (posInicial == 3 && posFinal == 1)
            {
                Console.WriteLine("C=>A");
            }
            else if (posInicial == 1 && posFinal == 2)
            {
                Console.WriteLine("A=>B");
            }
            else if (posInicial == 2 && posFinal == 1)
            {
                Console.WriteLine("B=>A");
            }
        }
    }
}
